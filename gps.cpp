/**
  * gps.cpp- Mtx_lr
  * Author: lrpower <lr@maitianxin.com>
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gps.h"


long int Gps_Data_len = 0;
long int Gsensor_Data_len = 0;
//0:gps header 1:gps data 2:gsensor header 3:gsensor data
static long int tell[4]={-1,-1,-1,-1};

int getCKID(unsigned char *pData,int size,long int filetell,FILE *fp)
{
	for(int i=0;i<size;i++){
		if(MKTAG(pData[i],pData[i+1],pData[i+2],pData[i+3])==CKID_AVI_IDIT)
		{
			unsigned char date[19]={0};//%04d-%02d-%02d %02d:%02d:%02d
			memset(date,0,sizeof(date));
			memcpy(date,pData+i+4,19);
			printf("start time:%s\n",date);
		}
		else if(MKTAG(pData[i],pData[i+1],pData[i+2],pData[i+3])==CKID_AVI_GPS_HEADER)
		{
			tell[0] = filetell+i;
		}
		else if(MKTAG(pData[i],pData[i+1],pData[i+2],pData[i+3])==CKID_AVI_GPS_DATA)
		{
			tell[1] = filetell+i;
		}
		else if(MKTAG(pData[i],pData[i+1],pData[i+2],pData[i+3])==CKID_AVI_GSENSOR_HEADER)
		{
			tell[2] = filetell+i;
		}
		else if(MKTAG(pData[i],pData[i+1],pData[i+2],pData[i+3])==CKID_AVI_GSENSOR_DATA)
		{
			tell[3] = filetell+i;
		}
	}
	if(tell[0]>=0&&tell[1]>=0&&tell[2]>=0&&tell[3]>=0){
		Gps_Data_len = tell[2] - tell[1] - 8;
		fseek(fp,0,SEEK_END);
		Gsensor_Data_len = ftell(fp) - tell[3] - 4;
		return 1;
	}
	return 0;
}

int Gps_Info_Read(long int GpsSize,long int *TellInfo,FILE *fp)
{
	static MMP_ULONG    ulGPSCount = 0;
	ulGPSCount = GpsSize/GPS_DATA_LEN;

	GPSINFOCHUCK *pData = new GPSINFOCHUCK[DATA_LEN];

	//gps info
	fseek(fp,TellInfo[1]+4,SEEK_SET);//CKID_AVI_GPS_DATA
	memset(pData,0,sizeof(pData));
	size_t size = fread(pData,1,GpsSize,fp);

	pGPSInfoChuck sGPSInfo2Chuck = new GPSINFOCHUCK[ulGPSCount];
	memcpy(sGPSInfo2Chuck,pData,GpsSize);

	for(int i=0;i<ulGPSCount;i++){
		printf("%d: %04d-%02d-%02d %02d:%02d:%02d dwLat:%lf dwLon:%lf\n",i,sGPSInfo2Chuck[i].sUTC.byYear+2000,
																sGPSInfo2Chuck[i].sUTC.byMon,
																sGPSInfo2Chuck[i].sUTC.byDay,
																sGPSInfo2Chuck[i].sUTC.byHour,
																sGPSInfo2Chuck[i].sUTC.byMin,
																sGPSInfo2Chuck[i].sUTC.bySec,
																sGPSInfo2Chuck[i].dwLat,
																sGPSInfo2Chuck[i].dwLon);
	}

	delete[] sGPSInfo2Chuck;
	delete[] pData;
	return 0;
}

int Gsensor_Info_Read(long int GsensorSize,long int *TellInfo,FILE *fp)
{
	static MMP_ULONG    ulGsensorCount = 0;
	ulGsensorCount = GsensorSize/GSENSOR_DATA_LEN;

	GSNR_INFOCHUCK *pData = new GSNR_INFOCHUCK[DATA_LEN];
	//gsensor info
	fseek(fp,TellInfo[3]+4,SEEK_SET);//CKID_AVI_GSENSOR_DATA
	memset(pData,0,sizeof(pData));
	size_t size = fread(pData,1,GsensorSize,fp);

	PGSNR_INFOCHUCK sGsensorInfo2Chuck = new GSNR_INFOCHUCK[ulGsensorCount];
	memcpy(sGsensorInfo2Chuck,pData,GsensorSize);

	for(int i=0;i<ulGsensorCount;i++){
		printf("%d: x:%u y:%u z:%u\n",i,sGsensorInfo2Chuck[i].sbX,sGsensorInfo2Chuck[i].sbY,sGsensorInfo2Chuck[i].sbZ);
	}

	delete[] sGsensorInfo2Chuck;
	delete[] pData;

	return 0;
}

int main(int argc,char *argv[])
{
	MMP_UBYTE *pData = new MMP_UBYTE[DATA_LEN];
	FILE *fp = fopen(argv[1],"rb");

	long int len = DATA_LEN;
	fseek(fp,-len,SEEK_END);
	while(ftell(fp)!=0){
		long int filetell=ftell(fp);
		size_t size = fread(pData,1,DATA_LEN,fp);
		
		if(getCKID(pData,(int)size,filetell,fp))
		{
			break;
		}
		len += DATA_LEN;
		fseek(fp,-len,SEEK_END);
	}
	if(pData){
		delete[] pData;
	}

	Gps_Info_Read(Gps_Data_len,tell,fp);
	Gsensor_Info_Read(Gsensor_Data_len,tell,fp);
	fclose(fp);
	return 0;
}

