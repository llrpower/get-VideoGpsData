/**
  * gps.h- Mtx_lr
  * Author: lrpower <lr@maitianxin.com>
*/
#ifndef __GSP_H__
#define _GSP_H__

typedef signed short    GSNR_SBYTE;
typedef unsigned short  MMP_USHORT;
typedef unsigned char   MMP_UBYTE;
typedef int             MMP_LONG;
typedef unsigned int    MMP_ULONG;

#define DATA_LEN 1024*1024

#define MKTAG(a,b,c,d) ((d) | ((c) << 8) | ((b) << 16) | ((unsigned)(a) << 24))

#define CKID_AVI_IDIT			    MKTAG('I', 'D', 'I', 'T')
#define CKID_AVI_GPS_HEADER         MKTAG('g', 'p', 's', 'a')
#define CKID_AVI_GPS_DATA           MKTAG('g', 'p', 's', '0')
#define CKID_AVI_GSENSOR_HEADER     MKTAG('g', 's', 'e', 'a')
#define CKID_AVI_GSENSOR_DATA       MKTAG('g', 's', 'e', 'n')


typedef struct _GPSINFOCHUCKTIME
{
    MMP_UBYTE     byYear;       /**< Years since 1900 */
    MMP_UBYTE     byMon;        /**< Months since January - [0,11] */
    MMP_UBYTE     byDay;        /**< Day of the month - [1,31] */
    MMP_UBYTE     byHour;       /**< Hours since midnight - [0,23] */
    MMP_UBYTE     byMin;        /**< Minutes after the hour - [0,59] */
    MMP_UBYTE     bySec;        /**< Seconds after the minute - [0,59] */

}GPSINFOCHUCKTIME;

typedef struct _GPSINFOCHUCK
{
    double                  dwLat;      /**< Latitude in NDEG - +/-[degree][min].[sec/60] */
    double                  dwLon;      /**< Longitude in NDEG - +/-[degree][min].[sec/60] */
    MMP_LONG                lAlt;       /**< Altitude in meter +-:under/below sea level*/
    MMP_USHORT              usSpeed;    /**< Speed unit: km/h */
    GPSINFOCHUCKTIME        sUTC;       /**< UTC of position */
    MMP_UBYTE               ubDirection;/**< Clockwise degree from the North.*/
    MMP_UBYTE               ubFlag;     /**< Check if the GPS data is valid;*/
    MMP_UBYTE               ubVersion;  /**< Stuture Version*/
    MMP_UBYTE               ubReserved;

}GPSINFOCHUCK, *pGPSInfoChuck;

typedef struct _GSNRINFOCHUCK
{
    GSNR_SBYTE  sbX;    //X axis value
    GSNR_SBYTE  sbY;    //Y axis value
    GSNR_SBYTE  sbZ;    //Z axis value

}GSNR_INFOCHUCK, *PGSNR_INFOCHUCK;

#define GPS_DATA_LEN sizeof(GPSINFOCHUCK)
#define GSENSOR_DATA_LEN sizeof(GSNR_INFOCHUCK)

#endif//_GSP_H__